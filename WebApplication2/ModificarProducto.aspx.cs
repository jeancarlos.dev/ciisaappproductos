﻿using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace WebApplication2
{
    public partial class ModificarProducto : System.Web.UI.Page
    {

        private string QUERY_MODIFICAR_PRODUCTO = "update productos set codigo=@codigo, nombre_producto=@nombre, precio=@precio, descripcion=@descripcion where codigo=@codigo;";
        private string QUERY_ELIMINAR_PRODUCTO = "delete from productos where codigo=@codigo;";

        //Metodo que valida solo campos vacios
        private Boolean ValidarCamposVacios()
        {


            if (this.txtCodigo.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el código");
                return false;
            }
            if (this.txtNombre.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el nombre");
                return false;
            }
            if (this.txtPrecio.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el precio");
                return false;
            }


            if (this.txtDescripcion.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar una descripción");
                return false;
            }

            return true;
        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres()
        {

            if (this.txtCodigo.Text.Length > 10)
            {
                this.Mensaje("Por favor, El código no debe superar los 10 caracteres");
                return false;
            }

            if (this.txtNombre.Text.Length > 30)
            {
                this.Mensaje("Por favor, El nombre no debe superar los 30 caracteres");
                return false;
            }
            if (this.txtPrecio.Text.Length > 9)
            {
                this.Mensaje("Por favor, El precio no debe superar los 9 digitos");
                return false;
            }


            if (this.txtDescripcion.Text.Length > 250)
            {
                this.Mensaje("Por favor, La descripción no debe superar los 250 caracteres");
                return false;
            }

            return true;
        }

        //Metodo que valida el tipo de caracterer
        private bool ValidarCamposDatos(){


            // Validar que el valor del campo precio sea numérico
            string valorFormularioPrecio;
            bool esNumero;

            valorFormularioPrecio = txtPrecio.Text;
            esNumero = int.TryParse(valorFormularioPrecio, out _);

            if (esNumero == false)
            {
                this.Mensaje("En el campo precio, sólo se admiten números");
                return false;
            }

  
            return true;
        }

        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje)
        {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('" + mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }

        //Buscar producto por codigo y llenar formulario
        public void ValidarCodigoExistente(string codigo)
        {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
            sqlConnection.Open();

            string query = "select codigo, nombre_producto, precio, descripcion from productos where codigo=@codigo";

            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Parameters.AddWithValue("codigo", codigo);
            SqlDataReader producto = sqlCommand.ExecuteReader();

                while (producto.Read())
                {
                    txtCodigo.Text = (String) producto[0];
                    txtNombre.Text = (String) producto[1];
                    txtPrecio.Text = (String)Convert.ToInt32(producto[2]).ToString();
                    txtDescripcion.Text = (String) producto[3];
                }

        }

 
        private string modificarProducto(string codigo, string nombre, int precio, string descripcion) {

            SqlConnection sqlConnection = abrirConexion();
            SqlCommand sqlCommand = new SqlCommand(QUERY_MODIFICAR_PRODUCTO, sqlConnection);

            sqlCommand.Parameters.AddWithValue("codigo", codigo);
            sqlCommand.Parameters.AddWithValue("nombre", nombre);
            sqlCommand.Parameters.AddWithValue("precio", precio);
            sqlCommand.Parameters.AddWithValue("descripcion", descripcion);

            int rows = 0;
            string mensaje = "";

            try
            {
                rows = sqlCommand.ExecuteNonQuery();

                if (rows > 0)
                {
                    mensaje = "Modificado satisfactoriamente";

                }
                else
                {
                    mensaje = "Error al modificar producto";
                }

                sqlConnection.Close();

            }
            catch (InvalidCastException ex)
            {

                mensaje = ex.Message;


            }

            return mensaje;



        }

        protected string eliminarProducto(string codigo)
        {
            SqlConnection sqlConnection = abrirConexion();
            SqlCommand sqlCommand = new SqlCommand(QUERY_ELIMINAR_PRODUCTO, sqlConnection) ;
            sqlCommand.Parameters.AddWithValue("codigo", codigo);


            int rows = 0;
            string mensaje = "";

            try
            {
                rows = sqlCommand.ExecuteNonQuery();

                if (rows > 0)
                {
                    mensaje = "Eliminado satisfactoriamente";

                }
                else
                {
                    mensaje = "Error al modificar producto";
                }

                sqlConnection.Close();

            }
            catch (InvalidCastException ex)
            {

                mensaje = ex.Message;


            }

            return mensaje;
           

        }

        protected void BtnModificar(object sender, EventArgs e)
        {

            if (ValidarCamposVacios() && ValidarCamposLongitudCaracteres() && ValidarCamposDatos()) {
                string codigo = txtCodigo.Text;
                string nombre = txtNombre.Text;
                int precio = Convert.ToInt32(txtPrecio.Text);
                string descripcion = txtDescripcion.Text;

                string mensaje = "";
                mensaje = modificarProducto(codigo, nombre, precio, descripcion);
                limpiarCampos();
                this.Mensaje(mensaje);
            }


          
        }

        protected void btnEliminar(object sender, EventArgs e) {
            string codigo = txtCodigo.Text;
            string mensaje = "";
            mensaje = eliminarProducto(codigo);
            limpiarCampos();
            this.Mensaje(mensaje);
            
        }

        private void limpiarCampos() {
            txtCodigo.Text="";
            txtNombre.Text="";
            txtPrecio.Text="";
            txtDescripcion.Text="";
        }

        protected void btnBusqueda(object sender, EventArgs e)
        {

            String valor=txtBusqueda.Text;
            ValidarCodigoExistente(valor);
           

        }

        private SqlConnection abrirConexion() {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
            sqlConnection.Open();
            return sqlConnection;
        }
    }
}