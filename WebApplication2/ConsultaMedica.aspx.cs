﻿using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace WebApplication2
{
    public partial class ConsultaMedica : System.Web.UI.Page
    {

        public void btnConsultaMedicas(object sender, EventArgs e)
        {
            /*
            if (ValidarCamposVacios() && ValidarCamposLongitudCaracteres() && ValidarCamposDatos())
            {
                //Peticion para registrar el producto
                this.Mensaje("Consulta disponible");
            }
            */
            String busqueda = txtBusqueda.Text;
            String filtro= listaFiltro.SelectedValue;

            this.Mensaje("filtro: " + filtro + "busqueda: "+busqueda);

            busquedaSQL(filtro , busqueda);

        }


        

        private int busquedaSQL(String filtro, String busqueda) { 

            int rows = 0;
            
            try
            {

                SqlConnection sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=clinica;  Integrated Security=True";
                sqlConnection.Open();

                String query ="";

                if (filtro.Equals("medico")){
                    query = "select fecha, medico, centro, especialidad from horamedicas where medico = @busqueda;";
                }

 
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("filtro", filtro);
                sqlCommand.Parameters.AddWithValue("busqueda", busqueda);

                this.Mensaje("QUERY: " + query);


                rows = Convert.ToInt16(sqlCommand.ExecuteScalar());

                this.Mensaje("FILA: " + rows);

                sqlConnection.Close();

                return rows;

            }
            catch (SqlException ex)
            {
                Response.Write("Ha ocurrido un problema con la conexión  a base de datos." + ex.Message);
            }

            return rows;

        }

       

     

        //UTILIZACION DE CONDICIONALES

        //Metodo que valida solo campos vacios
        private Boolean ValidarCamposVacios()
        {

            /*
             
             if (this.TextBox1.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el código");
                return false;
            }
            if (this.TextBox3.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el nombre");
                return false;
            }
            

            return true;
             */

            return false;

        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres()
        {

            /*
              if (this.TextBox1.Text.Length > 10)
             {
                 this.Mensaje("Por favor, El código no debe superar los 10 caracteres");
                 return false;
             }

             if (this.TextBox3.Text.Length > 25)
             {
                 this.Mensaje("Por favor, El nombre no debe superar los 25 caracteres");
                 return false;
             }
             if (this.TextBox4.Text.Length > 10)
             {
                 this.Mensaje("Por favor, El precio no debe superar los 10 caracteres");
                 return false;
             }

             if (this.TextBox2.Text.Length > 10)
             {
                 this.Mensaje("Por favor, la cantidad no debe superar los 10 caracteres");
                 return false;
             }

             if (this.TextBox5.Text.Length > 250)
             {
                 this.Mensaje("Por favor, La descripción no debe superar los 250 caracteres");
                 return false;
             }

             return true;
             */
            return false;
        }

        //Metodo que valida el tipo de caracterer
        private bool ValidarCamposDatos(){
            /*

            // Validar que el valor del campo precio sea numérico
            string valorFormularioPrecio;
            bool esNumero;

            valorFormularioPrecio = TextBox4.Text;
            esNumero = int.TryParse(valorFormularioPrecio, out _);

            if (esNumero == false)
            {
                this.Mensaje("En el campo precio, sólo se admiten números");
                return false;
            }

             Validar que el valor del campo nombre sea string
            string valorFormularioNombre;

            valorFormularioNombre = TextBox3.Text;
            //esString = valorFormularioNombre is string;

            bool resultado = Regex.IsMatch(valorFormularioNombre, @"^[a-zA-Z]+$");
            if (!resultado)
            {
                this.mensaje("En el campo nombre, Sólo se admiten letras");
                return false;
            }

           
            string valorFormularioCantidad;
            bool esNumero1;

            valorFormularioCantidad = TextBox2.Text;
            esNumero1 = int.TryParse(valorFormularioCantidad, out _);

            if (esNumero1 == false)
            {
                this.Mensaje("En el campo cantidad, sólo se admiten números");
                return false;
            }


            return true;

            */

            return false;
        }





        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje)
        {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('" + mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }
    }
}