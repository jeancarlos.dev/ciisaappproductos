﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RegistrarProducto.aspx.cs" Inherits="WebApplication2.RegistrarProducto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <main>
        <div class="card">
            <h3>Registro de producto</h3>
            <div class="formulario1">
                <asp:TextBox placeholder="Ingrese código" ID="txtCodigo" runat="server"></asp:TextBox>
                <asp:TextBox placeholder="Ingrese nombre" ID="txtProducto" runat="server"></asp:TextBox>
                <asp:TextBox placeholder="Ingrese precio" ID="txtPrecio" runat="server"></asp:TextBox>
                <asp:TextBox placeholder="Ingrese descripción" ID="txtDescripcion" runat="server"></asp:TextBox>
                <asp:Button class="btnGuardar" ID="btnGuardar" runat="server" Text="Guardar" OnClick="BtnRegistrar" />
            </div>
        </div>

    </main>

</asp:Content>


