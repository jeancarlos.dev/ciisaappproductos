﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="BuscarProducto.aspx.cs" Inherits="WebApplication2.BuscarProducto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

    <main>
        
        <div class="card">
            <h3>Buscar producto</h3>
            <div style="display: grid;grid-gap: 20px;">

                <div class="formularioBusqueda">

                    <asp:DropDownList ID="listaFiltro" runat="server" >
                        <asp:ListItem Value="codigo" DataValueField="codigo">Código</asp:ListItem>
                        <asp:ListItem Value="nombre" DataValueField="nombreProducto">Nombre</asp:ListItem>
                        <asp:ListItem Value="sucursal" DataValueField="sucursal">Sucursal</asp:ListItem>
                    </asp:DropDownList>

                    <asp:TextBox style="width: -moz-available; width: -webkit-fill-available;" placeholder="Búsqueda" ID="TextBox5" runat="server"></asp:TextBox>
                    <asp:Button class="btnGuardar" ID="Button2" runat="server" Text="Buscar" OnClick="BtnBuscar" />
                </div>

                   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True">
        <Columns>
            <asp:BoundField DataField="codigo" HeaderText="codigo" SortExpression="codigo" />
            <asp:BoundField DataField="nombre_producto" HeaderText="nombre_producto" SortExpression="nombre_producto" />
            <asp:BoundField DataField="nombre_local" HeaderText="nombre_local" SortExpression="nombre_local" />
            <asp:BoundField DataField="stock" HeaderText="stock" SortExpression="stock" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ciisaConnectionString %>" SelectCommand="SELECT pro.codigo, pro.nombre_producto, loc.nombre_local, ls.stock
FROM locales_stock AS ls 
INNER JOIN locales AS loc ON ls.id_local = loc.id_local 
INNER JOIN productos AS pro ON ls.id_producto = pro.id_producto "></asp:SqlDataSource>

    
                

            </div>
        </div>
    </main>

    

 


    

 


 

 


    

 


</asp:Content>
