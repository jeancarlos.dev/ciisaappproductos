﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Data.SqlClient;

namespace WebApplication2
{
    public partial class ReservaMedica : System.Web.UI.Page
    {



        protected void Page_Load(object sender, EventArgs e)
        {
        
        }



        public int ValidarAsignacion(int idProducto, int idLocal)
        {

            int rows = 0;
            try
            {

                SqlConnection sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
                sqlConnection.Open();



                String query = "select id_producto, id_local from locales_stock where id_producto=@idProducto and id_local=@idLocal";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("idProducto", idProducto);
                sqlCommand.Parameters.AddWithValue("idLocal", idLocal);

                rows = Convert.ToInt16(sqlCommand.ExecuteScalar());

                sqlConnection.Close();


            }
            catch (SqlException ex)
            {
                Response.Write("Ha ocurrido un problema con la conexión  a base de datos." + ex.Message);
            }




            return rows;

        }


        public String asignarStockALocal(int idProducto, int idLocal, int stock) {

            int rows = 0;
            String mensaje = "";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
            sqlConnection.Open();

            String query = "";
            query = "insert into locales_stock values(@idProducto, @idLocal, @stock);";

            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Parameters.AddWithValue("idProducto", idProducto);
            sqlCommand.Parameters.AddWithValue("idLocal", idLocal);
            sqlCommand.Parameters.AddWithValue("stock", stock);

            rows=sqlCommand.ExecuteNonQuery();

            if (rows > 0)
            {
                mensaje = "Asignado satisfactoriamente";
                sqlConnection.Close();


            }
            else {
                mensaje = "Error al asignar stock al local";
            }

            return mensaje;

        }


        /*


        protected void BtnAsignar(object sender, EventArgs e)
        {



            //Variables que almacenan temporalmente los valores obtenidos del formulario.
            int idLocal;
            int idProducto;
            int stock;

            idProducto = int.Parse(listaProductos.SelectedValue);
            idLocal = int.Parse(listaLocales.SelectedValue);
            stock = int.Parse(txtStock.Text);


            String mensaje="";
            int asignacion = 0;
            asignacion = ValidarAsignacion(idProducto, idLocal);

            if (asignacion != 0)
            {
                mensaje = "Ya esta asignado el producto a esa sucursal.";
                this.Mensaje(mensaje);
            }
            else {


                mensaje = asignarStockALocal(idProducto, idLocal, stock);
                this.Mensaje(mensaje);
               

            }



         

          


            
         
            

        }


        //Metodo que valida solo campos vacios
        private Boolean ValidarCamposVacios()
        {


            if (this.txtStock.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el stock");
                return false;
            }

            return true;
        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres()
        {

            if (this.txtStock.Text.Length > 25)
            {
                this.Mensaje("Por favor, El stock no debe superar los 25 caracteres");
                return false;
            }


            return true;
        }

        //Metodo que valida el tipo de caracterer
        private Boolean ValidarCamposDatos()
        {

            // Validar que el valor del campo stock sea numérico
            string valorFormularioStock;
            bool esNumero;

            valorFormularioStock = txtStock.Text;
            esNumero = int.TryParse(valorFormularioStock, out _);

            if (esNumero == false)
            {
                this.Mensaje("En el campo stock, sólo se admiten números");
                return false;
            }

            return true;
        }

        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje)
        {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('" + mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }

        */

        
    }


}