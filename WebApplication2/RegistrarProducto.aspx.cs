﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace WebApplication2
{
    public partial class RegistrarProducto : System.Web.UI.Page
    {
    
        //Metodo que realiza persistencia con la base de datos.
        public String Registrar(String codigo, String producto,int precio, String descripcion) {

            int rows = 0;
            String mensaje = "";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
            sqlConnection.Open();
    


            String query = "insert into productos (codigo, nombre_producto,precio, descripcion) values(@codigo, @producto, @precio, @descripcion);";

            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Parameters.AddWithValue("codigo", codigo);
            sqlCommand.Parameters.AddWithValue("producto", producto);
            sqlCommand.Parameters.AddWithValue("precio", precio);
            sqlCommand.Parameters.AddWithValue("descripcion", descripcion);


            try
            {
                rows = sqlCommand.ExecuteNonQuery();

                if (rows > 0)
                {
                    mensaje = "Registrado satisfactoriamente";

                }
                else
                {
                    mensaje = "Error al registrar producto";
                }

                sqlConnection.Close();

            }
            catch (InvalidCastException ex) {

                mensaje = ex.Message;


            } 


          

            return mensaje;

        }

        //Metodo que valida si el codigo existe en base de datos
        public int ValidarCodigoExistente(String codigo) {

            int rows = 0;
            try {

                SqlConnection sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
                sqlConnection.Open();

                

                String query = "select codigo from productos where codigo=@codigo";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("codigo", codigo);

                rows = Convert.ToInt16(sqlCommand.ExecuteScalar());

                sqlConnection.Close();


            } catch (SqlException ex) {
                Response.Write("Ha ocurrido un problema con la conexión  a base de datos."+ex.Message);
            }
           

          

            return rows;

        }

        //Metodo que valida si el nombre del producto existe en base de datos
        public int ValidarProductoExistente(String nombreProducto)
        {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=ciisa;  Integrated Security=True";
            sqlConnection.Open();

            int rows = 0;

            String query = "select nombre_producto from productos where nombre_producto=@nombre_producto";

            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Parameters.AddWithValue("nombre_producto", nombreProducto);

            rows = Convert.ToInt16(sqlCommand.ExecuteScalar());
          

            sqlConnection.Close();

            return rows;
        }

        //Metodo que ejecuta el proceso para registrar un producto
        protected void BtnRegistrar(object sender, EventArgs e)
        {

            String mensaje="";

            //1) Valida  los campos del formulario
            if (ValidarCamposVacios() && ValidarCamposLongitudCaracteres() && ValidarCamposDatos()) {


                //2 Se obtienen los datos del formulario
                String codigo;
                String producto;
                String descripcion;
                int precio;

                codigo = txtCodigo.Text;
                producto = txtProducto.Text;
                descripcion = txtDescripcion.Text;
                precio =Convert.ToInt32(txtPrecio.Text);



                //3- Valida si existe el codigo de producto existe bd, si no existe entonces permite seguir el flujo.

                int codigoProducto = 0;
                codigoProducto = ValidarCodigoExistente(codigo);

                if (codigoProducto != 0)
                {
                    mensaje = "El código de producto ya existe, por favor ingresar otro.";
                    this.Mensaje(mensaje);
                }


                //Utilizando excepciones
                try
                {

                    //4- Valida si existe el nombre de producto existe bd, si no existe entonces permite seguir el flujo.
                    int nombreProducto = 0;
                    nombreProducto = ValidarProductoExistente(producto);

                    if (nombreProducto != 0)
                    {
                        mensaje = "El nombre de producto ya existe, por favor ingresar otro.";
                        this.Mensaje(mensaje);
                    }

                    if (codigoProducto == 0 && nombreProducto == 0)
                    {
                        mensaje = Registrar(codigo, producto, precio, descripcion);
                        this.Mensaje(mensaje);
                        limpiarCampos();
                    }

                }
                catch (Exception ex)
                {

                    this.Mensaje("El nombre de producto ya existe, por favor ingrese otro. "+ex.Message);
                }






            }





        }

        //Metodo que limpia los campos luego de realizar la insercion del producto
        private void limpiarCampos() {
            this.txtCodigo.Focus();
            this.txtCodigo.Text = "";
            this.txtProducto.Text = "";
            this.txtPrecio.Text = "";
            this.txtDescripcion.Text = "";
        }

        //Metodo que valida solo campos vacios
        private Boolean ValidarCamposVacios() {
           
            if (this.txtCodigo.Text.Length <= 0) {
                this.Mensaje("Por favor, ingresar el código");
                return false;
            }
            if (this.txtProducto.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el nombre");
                return false;
            }
            if (this.txtPrecio.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el precio");
                return false;
            }
            if (this.txtDescripcion.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar una descripción");
                return false;
            }

            

            return true;
        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres() {

            if (this.txtCodigo.Text.Length > 10) {
                this.Mensaje("Por favor, El código no debe superar los 10 caracteres");
                return false;
            }

            if (this.txtProducto.Text.Length > 30)
            {
                this.Mensaje("Por favor, El nombre no debe superar los 30 caracteres");
                return false;
            }


 
            if (this.txtPrecio.Text.Length > 9)
            {
                this.Mensaje("Por favor, El precio no debe superar los 9 digitos");
                return false;
            }

            if (this.txtDescripcion.Text.Length > 250)
            {
                this.Mensaje("Por favor, La descripción no debe superar los 250 caracteres");
                return false;
            }

          
            return true;
        }
        
        //Metodo que valida el tipo de caracterer
        private Boolean ValidarCamposDatos() {

        // Validar que el valor del campo precio sea numérico
            string valorFormularioPrecio;
            bool esNumero;

            valorFormularioPrecio = txtPrecio.Text;
            esNumero = int.TryParse(valorFormularioPrecio, out _);

            if (esNumero == false)
            {
                this.Mensaje("En el campo precio, sólo se admiten números");
                return false;
            }

        

            return true;
        }

        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje) {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('"+ mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }

    }
}