﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Asignacion.aspx.cs" Inherits="WebApplication2.Asignacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <main>
           <div class="card">

             <h3>Asignación de productos</h3>

            <div class="formulario1">

                <asp:DropDownList ID="listaLocales" runat="server" DataSourceID="SqlDataSource1" DataTextField="nombre_local" DataValueField="id_local" ></asp:DropDownList>

                <asp:DropDownList ID="listaProductos" runat="server" DataSourceID="TABLA_PRODUCTOS" DataTextField="nombre_producto" DataValueField="id_producto" ></asp:DropDownList>




                <asp:SqlDataSource ID="TABLA_PRODUCTOS" runat="server" ConnectionString="<%$ ConnectionStrings:ciisaConnectionString %>" SelectCommand="SELECT [id_producto], [nombre_producto] FROM [productos] WHERE ([eliminado] = @eliminado)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="eliminado" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ciisaConnectionString %>" SelectCommand="SELECT [id_local], [nombre_local] FROM [locales]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>




                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ciisaConnectionString %>" SelectCommand="SELECT [id_local], [nombre_local] FROM [locales] WHERE ([eliminado] = @eliminado2)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="eliminado2" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:TextBox placeholder="Ingrese el Stock" ID="txtStock" runat="server"></asp:TextBox>

            
                <asp:Button class="btnGuardar" ID="Button2" runat="server" Text="Guardar" OnClick="BtnAsignar" />
            </div>

        </div>
    </main>
  





</asp:Content>
