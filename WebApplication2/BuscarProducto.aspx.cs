﻿using System;
using System.Web.UI;
using System.Text.RegularExpressions;

namespace WebApplication2
{
    public partial class BuscarProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            /*
            //UTILIZACION DE CICLOS
            string[] stringArray = new string[6];
            stringArray[0] = "Prueba1";
            stringArray[1] = "Prueba2";
            stringArray[2] = "Prueba3";
            stringArray[3] = "Prueba3";
            stringArray[4] = "Prueba5";
            stringArray[5] = "Prueba6";

            for (int i = 0; i < stringArray.Length; i++)
            {
                Console.WriteLine("DATO : " + stringArray[i]);
            }
            */


        }

        protected void BtnBuscar(object sender, EventArgs e)
        {
            if (ValidarCamposVacios() && ValidarCamposLongitudCaracteres() && ValidarCamposDatos())
            {

                //Peticion para registrar el producto
                String filtro =listaFiltro.SelectedValue;




                this.Mensaje("Producto encontrado satisfactoriamente"+ filtro);


            }

        }

        public void buscarProducto(String filtro) {

            //En desarrollo...

        
        }


        //Metodo que valida solo campos vacios
        private bool ValidarCamposVacios()
        {
            if (this.TextBox5.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar el producto que quiere buscar");
                return false;
            }
            return true;
        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres()
        {

            if (this.TextBox5.Text.Length > 10)
            {
                this.Mensaje("Por favor, el nombre del producto no debe superar los 10 caracteres");
                return false;
            }


            return true;
        }

        //Metodo que valida el tipo de caracterer
        private Boolean ValidarCamposDatos()
        {

            // Validar que el valor del campo nombre sea string
            string valorFormularioNombre;

            valorFormularioNombre = TextBox5.Text;
            //esString = valorFormularioNombre is string;

            bool resultado = Regex.IsMatch(valorFormularioNombre, @"^[a-zA-Z]+$");
            if (!resultado)
            {
                this.Mensaje("En el campo nombre, Sólo se admiten letras");
                return false;
            }

            return true;
        }

        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje)
        {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('" + mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }
    }
}