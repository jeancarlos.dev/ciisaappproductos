﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace WebApplication2
{
    public partial class RegistrarPaciente : System.Web.UI.Page
    {
        
        //Metodo que realiza persistencia con la base de datos.
        public String RegistrarPacientes(String run, String nombres, String apellidos, String direccion, String clave, String prevision) {

            int rows = 0;
            String mensaje = "";

            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=clinica;  Integrated Security=True";
            sqlConnection.Open();

   
            String query = "insert into pacientes (run, nombres, apellidos, direccion, clave, prevision) values(@run, @nombres, @apellidos,@direccion, @clave, @prevision);";

            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Parameters.AddWithValue("run", run);
            sqlCommand.Parameters.AddWithValue("nombres", nombres);
            sqlCommand.Parameters.AddWithValue("apellidos", apellidos);
            sqlCommand.Parameters.AddWithValue("direccion", direccion);
            sqlCommand.Parameters.AddWithValue("clave", clave);
            sqlCommand.Parameters.AddWithValue("prevision", prevision);


            try
            {
                rows = sqlCommand.ExecuteNonQuery();

                if (rows > 0)
                {
                    mensaje = "Registrado satisfactoriamente";

                }
                else
                {
                    mensaje = "Error al registrar paciente";
                }

                sqlConnection.Close();

            }
            catch (InvalidCastException ex) {

                mensaje = ex.Message;


            } 


          

            return mensaje;

        }

        //Metodo que valida si el codigo existe en base de datos
        public int ValidarRunExistente(String run) {

            int rows = 0;
            try {

                SqlConnection sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = @"Data Source=DESKTOP-DAMTN31\MSSQLSERVER04; Initial Catalog=clinica;  Integrated Security=True";
                sqlConnection.Open();

    

                String query = "select run from pacientes where run=@run";



                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("run", run.Trim());


                rows = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    

                sqlConnection.Close();


            } catch (SqlException ex) {
                Response.Write("Ha ocurrido un problema con la conexión  a base de datos."+ex.Message);
            }




            return rows ;

        }

        //Metodo que ejecuta el proceso para registrar un producto
        protected void BtnRegistrar(object sender, EventArgs e)
        {

            String mensaje="";


   

            //1) Valida  los campos del formulario
            if (ValidarCamposVacios() && ValidarCamposLongitudCaracteres() && ValidarCamposDatos()) { 


                //2 Se obtienen los datos del formulario
                String run = txtRun.Text;
                String nombres = txtNombres.Text;
                String apellidos = txtApellidos.Text;
                String direccion = txtDireccion.Text;
                String clave = txtClave.Text;
                String prevision = txtPrevision.Text;


                //3- Valida si existe el run del paciente existe bd, si no existe entonces permite seguir el flujo.

                int runPaciente = 0;
                runPaciente = ValidarRunExistente(run);

                if (runPaciente != 0)
                {
                    mensaje = "El run ya existe, por favor ingresar otro.";
                    this.Mensaje(mensaje);
                }


            if (runPaciente == 0)
            {
                //Utilizando excepciones
                try
                {
                        
                           mensaje = RegistrarPacientes(run, nombres, apellidos, direccion, clave, prevision);
                    this.Mensaje(mensaje);
                    limpiarCampos();


                }
                catch (Exception ex)
                {

                    this.Mensaje("Error al ingresar el paciente " + ex.Message);
                }
            
            }


         






            }





        }

        //Metodo que limpia los campos luego de realizar la insercion del producto
        private void limpiarCampos() {
            this.txtRun.Focus();
            this.txtApellidos.Text = "";
            this.txtNombres.Text = "";
            this.txtDireccion.Text = "";
            this.txtClave.Text = "";
            this.txtPrevision.Text = "";
        }

        //Metodo que valida solo campos vacios
        private Boolean ValidarCamposVacios() {
           
            if (this.txtRun.Text.Length <= 0) {
                this.Mensaje("Por favor, ingresar su run");
                return false;
            }
            if (this.txtNombres.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingrese sus nombres");
                return false;
            }

            if (this.txtApellidos.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar sus apellidos");
                return false;
            }

            if (this.txtDireccion.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar una dirección");
                return false;
            }

            if (this.txtClave.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar una clave");
                return false;
            }

            if (this.txtPrevision.Text.Length <= 0)
            {
                this.Mensaje("Por favor, ingresar una previsión");
                return false;
            }

            return true;
        }

        //Metodo que valida la longitud de los caracteres
        private Boolean ValidarCamposLongitudCaracteres() {

            if (this.txtRun.Text.Length > 12) {
                this.Mensaje("Por favor, El código no debe superar los 12 caracteres");
                return false;
            }

            if (this.txtNombres.Text.Length > 50)
            {
                this.Mensaje("Por favor, El nombre no debe superar los 50 caracteres");
                return false;
            }

            if (this.txtApellidos.Text.Length > 50)
            {
                this.Mensaje("Por favor, El apellido no debe superar los 50 caracteres");
                return false;
            }
            if (this.txtDireccion.Text.Length > 250)
            {
                this.Mensaje("Por favor, La dirección no debe superar los 250 caracteres");
                return false;
            }
            if (this.txtClave.Text.Length >10)
            {
                this.Mensaje("Por favor, La dirección no debe superar los 10 caracteres");
                return false;
            }


            if (this.txtPrevision.Text.Length > 50)
            {
                this.Mensaje("Por favor, La previsión no debe superar los 50 caracteres");
                return false;
            }

            return true;
        }

        //Metodo que valida el tipo de caracterer
        private Boolean ValidarCamposDatos()
        {

            //Validarue el valor del campo precio sea numérico

            string valorFormularioRun;
                bool esNumero;

            valorFormularioRun = txtRun.Text;
                esNumero = int.TryParse(valorFormularioRun, out _);

                if (esNumero == false)
                {
                this.Mensaje("En el campo run , solo se admiten números");
                    return false;
                }




                // Validar que el valor del campo nombre sea string
           string valorFormularioNombre;
            string valorFormularioApellidos;
            string valorFormularioPrevision;


            valorFormularioNombre = txtNombres.Text;
            valorFormularioApellidos = txtApellidos.Text;
            valorFormularioPrevision = txtPrevision.Text;


            bool resultado =
                Regex.IsMatch(valorFormularioNombre, @"^[a-zA-Z]+$") &&
                Regex.IsMatch(valorFormularioApellidos, @"^[a-zA-Z]+$") &&
                Regex.IsMatch(valorFormularioPrevision, @"^[a-zA-Z]+$");
                if (!resultado)
                {
                    this.Mensaje("En el campo nombre, Sólo se admiten letras");
                    return false;
                }

                return true;
           

         
        }

        //Metodo generico para poder mostrar mensaje en pantalla utilizando javascript
        private void Mensaje(string mensaje) {
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
            {
                String cstext = "alert('"+ mensaje + "');";
                cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
            }
        }

    }
}